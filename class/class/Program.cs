﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @class
{
    class car
    {
        private string color;
        public string name;
        public car(string _name, string _color)
        {
            name = _name;
            color = _color;
        }
        public override string ToString()
        {
            return name.ToString()+" "+color.ToString();
        }
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            car car = new car("bmw", "red");
            Console.WriteLine(car);
            Console.ReadKey();
        }
    }
}
