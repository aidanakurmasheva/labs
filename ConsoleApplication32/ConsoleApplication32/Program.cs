﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = File.CreateText(@"c:\newfile.txt");
            writer.WriteLine("This is my new file");
            writer.WriteLine("do you like it?");
            writer.Close();
        }
    }
}
