﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConsoleApplication40
{
    class LevenshteinDistance
    {

        public static int Compute(string x, string y)
        {
            int n = x.Length;
            int m = y.Length;
            int[,] d = new int[n + 1, m + 1];
            for (int i = 0; i <= n; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j <= m; j++)
            {
                d[0, j] = j;
            }
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int c = (y[j - 1] == x[i - 1]) ? 0 : 2;
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + c);
                }
            }
            return d[n, m];
        }
    }
    class Program
    {
        static void Main(string[] args)
        {


            StreamReader sr = new StreamReader(@"C:\Users\Fujitsu\Desktop\words.txt");
            StreamReader tr = new StreamReader(@"C:\Users\Fujitsu\Desktop\text.txt");
            string[] dict = new string[60000];

            string[] word;
            int d_size = 0;
            while (sr.Peek() >= 0)
            {


                dict[d_size] = sr.ReadLine();
                d_size++;

            }
            int c = 0;
            string s = tr.ReadToEnd();
            word = s.Split(' ');
            c = word.Length;
            int p = 6000000, k;
            Console.WriteLine(c);
            for (int i = 0; i < c; i++)
            {
                p = 6000000;
                for (int j = 0; j < d_size; j++)
                {
                    if (LevenshteinDistance.Compute(word[i], dict[j]) < p)
                    {
                        p = LevenshteinDistance.Compute(word[i], dict[j]);


                    }
                }
                for (int j = 0; j < d_size; j++){
                    if (LevenshteinDistance.Compute(word[i], dict[j]) == p)
                    {
                        Console.WriteLine(word[i] + " " + dict[j]);
                    }

                }


            }


            Console.ReadKey();
        }
    }
}