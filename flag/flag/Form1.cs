﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace flag
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g;
            g = this.CreateGraphics();
            Pen pen = new Pen(Color.DodgerBlue, 1);
            Brush brush = new SolidBrush(Color.DodgerBlue);
            pen.DashStyle = DashStyle.Solid;
            g.DrawRectangle(pen, 10, 10, 410, 250);
            g.FillRectangle(brush, 10, 10, 410, 250);
            Pen pen1 = new Pen(Color.Gold, 30);
            
            pen1.DashStyle = DashStyle.Solid;
            g.DrawLine(pen1, 100, 10, 100, 260);
            g.DrawLine(pen1, 10, 100, 420, 100);
            
        }
    }
}
