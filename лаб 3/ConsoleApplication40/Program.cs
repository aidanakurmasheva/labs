﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConsoleApplication40
{
    static class LevenshteinDistance
    {
        
        public static int Compute(string x, string y)
        {
            int n = x.Length;
            int m = y.Length;
            int[,] d = new int[n + 1, m + 1];
            for (int i = 0; i <= n; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j <= m; j++)
            {
                d[0, j] = j;
            }
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int c = (y[j - 1] == x[i - 1]) ? 0 : 2;
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + c);
                }
            }
            return d[n, m];
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            string s;
            string k="aak";
            int p = k.Length;
            using (StreamReader reader = new StreamReader(@"C:\Users\Fujitsu\Desktop\words.txt"))
            {
                while ((reader.ReadLine()) != null)
                {
                    s = reader.ReadLine();

                    if (LevenshteinDistance.Compute(k, s) <= 2)
                         
                    Console.WriteLine( s);
                    
                }
            }
            Console.ReadKey();
        }
    }
}
