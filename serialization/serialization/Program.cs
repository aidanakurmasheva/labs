﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace serialization
{
    [XmlRoot ("student")]
    public class student
    {
        [XmlIgnore]
        public string surname;
        
        public double gpa;
        

        public string name;
        
        public string gender;
        
        
            
        
        
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            student stu = new student();
            stu.surname = "Kurmasheva";
            stu.name = "Aidana";
            stu.gpa = 4.0;
            SerializeToXML(stu);
        }

        static public void SerializeToXML(student stu)
        {
            
            FileStream fs = new FileStream(@"C:\Users\Fujitsu\Desktop\Serializedstudent.XML", FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(student));
            serializer.Serialize(fs, stu);
            fs.Close();

        }
    }
}
