﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication13
{   

    struct bignumber
    {
        public int[] a;
        public int size;
        public bignumber(string s)
        {
            a = new int[1000];
            size = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }
        public override string ToString()
        {
            string s = "";
            for (int i = size - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }
        public static bignumber operator -(bignumber b, bignumber c)
        {
            
            int m = Math.Max(b.size, c.size);
            
            if (b.size == c.size)
            {

                int p = 0;
                if (b.a[b.size - 1] == c.a[c.size - 1])
                {
                    for (int i = m; i > 0; i--)
                    {
                        if (b.a[i] == c.a[i])
                        {
                            p++;
                        }
                        else
                        {
                            break;
                        }
                    }



                    b.size = b.size - p + 1;
                    c.size = c.size - p + 1;
                    if (b.a[b.size - 1] == c.a[c.size - 1])//Я добавила еще один условный оператор.Если последние цифры равны,он будет выдавать всего один ноль.
                    {
                        for (int i = 0; i < b.size;i++ )
                            b.a[i] = 0;
                    }
                    else if (b.a[b.size - 1] > c.a[c.size - 1])
                    {
                        for (int i = 0; i < m; i++)
                        {
                            if (b.a[i] >= c.a[i])
                            {
                                b.a[i] = b.a[i] - c.a[i];
                            }
                            else
                            {
                                b.a[i] = (b.a[i] + 10) - c.a[i];
                                b.a[i + 1] = b.a[i + 1] - 1;
                            }
                        }
                    }
                    else if (b.a[b.size - 1] < c.a[c.size - 1])
                    {
                        int s;
                        for (int i = 0; i < m; i++)
                        {
                            s = b.a[i];
                            b.a[i] = c.a[i];
                            c.a[i] = s;
                        }
                        for (int i = 0; i < m; i++)
                        {

                            if (b.a[i] >= c.a[i])
                            {
                                b.a[i] = b.a[i] - c.a[i];
                            }
                            else
                            {
                                b.a[i] = (b.a[i] + 10) - c.a[i];
                                b.a[i + 1] = b.a[i + 1] - 1;
                            }
                        }
                        int k = 0;
                        for (int i = b.size - 1; i > 0; i--)
                        {
                            if (b.a[i] == 0)
                            {
                                k++;

                            }
                            else
                                break;
                        }
                        b.size = b.size - k;
                        b.a[b.size - 1] = b.a[b.size - 1] * -1;


                    }


                }
                else if (b.a[b.size - 1] > c.a[c.size - 1])
                {
                    for (int i = 0; i < m; i++)
                    {
                        if (b.a[i] >= c.a[i])
                        {
                            b.a[i] = b.a[i] - c.a[i];
                        }
                        else
                        {
                            b.a[i] = (b.a[i] + 10) - c.a[i];
                            b.a[i + 1] = b.a[i + 1] - 1;
                        }
                    }
                    for (int i = m - 1; i > 0; i--)
                    {

                        if (b.a[i] == 0)
                        {
                            b.size--;

                        }
                        else
                            break;
                    }
                }

                else if (b.a[b.size - 1] < c.a[c.size - 1])
                {
                    int s;
                    for (int i = 0; i < m; i++)
                    {
                        s = b.a[i];
                        b.a[i] = c.a[i];
                        c.a[i] = s;
                    }
                    for (int i = 0; i < m; i++)
                    {

                        if (b.a[i] >= c.a[i])
                        {
                            b.a[i] = b.a[i] - c.a[i];
                        }
                        else
                        {
                            b.a[i] = (b.a[i] + 10) - c.a[i];
                            b.a[i + 1] = b.a[i + 1] - 1;
                        }
                    }
                    int k = 0;
                    for (int i = b.size - 1; i > 0; i--)
                    {
                        if (b.a[i] == 0)
                        {
                            k++;

                        }
                        else
                            break;
                    }
                    b.size = b.size - k;
                    b.a[b.size - 1] = b.a[b.size - 1] * -1;


                }

                
            }
            else if (b.size > c.size)
            {
                for (int i = 0; i < m; i++)
                {
                    if (b.a[i] >= c.a[i])
                    {
                        b.a[i] = b.a[i] - c.a[i];
                    }
                    else
                    {
                        b.a[i] = (b.a[i] + 10) - c.a[i];
                        b.a[i + 1] = b.a[i + 1] - 1;
                    }

                }
                for (int i = m - 1; i > 0; i--)
                {

                    if (b.a[i] == 0)
                    {
                        b.size--;

                    }
                    else
                        break;
                }


            }
            else
            {
                for (int i = 0; i < m - 1; i++)
                {
                    if (b.a[i] <= c.a[i])
                    {
                        c.a[i] = c.a[i] - b.a[i];
                    }
                    else
                    {
                        c.a[i] = (c.a[i] + 10) - b.a[i];
                        c.a[i + 1] = c.a[i + 1] - 1;
                    }
                }

                for (int i = m - 1; i > 0; i--)
                {

                    if (c.a[i] == 0)
                    {
                        c.size--;

                    }
                    else
                        break;
                }
                c.a[c.size - 1] = c.a[c.size - 1] * -1;
                return c;
            }

            return b;
            
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            bignumber b = new bignumber("1555");
            Console.WriteLine(b);
            bignumber c = new bignumber("1555");
            Console.WriteLine(c);
            bignumber p = new bignumber();
            p = b - c;
            Console.WriteLine(p);
            Console.ReadKey();
        }
    }
}