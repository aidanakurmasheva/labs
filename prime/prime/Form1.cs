﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBox1.Text);
            bool prime = true;
            for (int j = 2; j * j <= a; j++)
            {
                    if (a % j == 0)
                    {
                        prime = false;
                        break;
                        
                    }
                    
                   
            }

                
            if (prime)
            {
                label1.Text = "It's prime number";
            }
            else
                label1.Text = "It's not prime number";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
