﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace ConsoleApplication8
{   

    class Program
    {
        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }   
        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }
        static void Main(string[] args)
        {
            string k = "01111-1254";
            if (IsPhone(k) == true)
            {
                Console.WriteLine("is the phone number");
            }
            else if(IsZip(k)==true)
            {
                Console.WriteLine("is the zipcode");
            }
            else
                Console.WriteLine("not recognized");
            Console.ReadKey();
        }
    }
}
