﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstractclass
{
    public abstract class figure
    {
        public abstract void perimeter();
        public abstract void area();
        public abstract void info();
    }
    public class rectangle : figure
    {
        int a;
        int b;
        public rectangle(int _a, int _b)
        {
            a = _a;
            b = _b;
        }
        public override string ToString()
        {
            return a.ToString() + " " + b.ToString();
        }
        public override void perimeter()
        {
            int p = (2 * a) + (2 * b);
            Console.WriteLine(p);
        }
        public override void area()
        {
            int ar = a * b;
            Console.WriteLine(ar);
        }
        public override void info()
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine("it is a rectangle");
        }
    }
    public class triangle : figure
    {
        int x;
        int y;
        int z;
        public triangle(int _x, int _y,int _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString()+" "+z.ToString();
        }
        public override void perimeter()
        {
            int p = x + y + z;
            Console.WriteLine(p);
        }
        public override void area()
        {
            int k = (x + y + z)/2;
            double ar = Math.Sqrt(k * (k - x) * (k - y) * (k - z));
            Console.WriteLine(ar);
        }
        public override void info()
        {
            Console.WriteLine(x);
            Console.WriteLine(y);
            Console.WriteLine(z);
            Console.WriteLine("it is a triangle");
        }
    }
    public class circle : figure
    {
        int r;
        public circle(int _r)
        {
            r = _r;
        }
        public override string ToString()
        {
            return r.ToString();
        }
        public override void perimeter()
        {
            double p = 2*r*Math.PI;
            Console.WriteLine(p);
        }
        public override void area()
        {
            double ar =Math.PI*r*r;
            Console.WriteLine(ar);
        }
        public override void info()
        {
            Console.WriteLine(r);
            Console.WriteLine("it is a circle");
        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            rectangle myrec = new rectangle(5, 6);
            myrec.perimeter();
            myrec.area();
            myrec.info();
            circle mycir = new circle(5);
            mycir.perimeter();
            mycir.area();
            mycir.info();
            triangle mytri = new triangle(5, 6, 7);
            mytri.perimeter();
            mytri.area();
            mytri.info();
            Console.ReadKey();
            

        }
    }
}
