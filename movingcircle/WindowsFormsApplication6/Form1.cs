﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication6
{
    public partial class Form1 : Form
    {

        enum Pos
        {
            Left,Right,Up,Down,
        }   
        private int x;
        private int y;
        private Pos objpos;
        public Form1()
        {
            InitializeComponent();
            x = 50;
            y = 50;
            objpos = Pos.Right;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            
            Brush brush = new SolidBrush(Color.DodgerBlue);
            
            e.Graphics.FillEllipse(brush, x, y, 50, 50);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (objpos == Pos.Right)
            {
                x = x + 10;
            }
            else if (objpos == Pos.Left)
            {
                x = x - 10;
            }
            else if (objpos == Pos.Up)
            {
                y = y - 10;
            }
            else if (objpos == Pos.Down)
            {
                y = y+ 10;
            }
            
            Invalidate();

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                objpos = Pos.Left;
            }
            else if (e.KeyCode == Keys.Right)
            {
                objpos = Pos.Right;
            }
            else if (e.KeyCode == Keys.Up)
            {
                objpos = Pos.Up;
            }
            else if (e.KeyCode == Keys.Down)
            {
                objpos = Pos.Down;
            }
        }
    }
}
