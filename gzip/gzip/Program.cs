﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace gzip
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream sourceFile = File.OpenRead(@"C:\Users\Fujitsu\Desktop\in.txt");
            FileStream destFile = File.Create(@"C:\Users\Fujitsu\Desktop\out.txt");
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
        }
    }
}
