﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seabattle
{
    class ship
    {
        int dx;
        int dy;
        string dir;
        int len;
        public ship(int _dx,int _dy,string _dir,int _len)
        {
            dx = _dx;
            dy = _dy;
            dir=_dir;
            len=_len;
        }
        public override string ToString()
        {
             return dx.ToString()+" "+dy.ToString()+" "+dir.ToString()+" "+len.ToString();
        }
    }
}
