﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace gzip
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream sourceFile = File.OpenRead(@"C:\Users\Fujitsu\Desktop\in.txt");
            FileStream destFile = File.Create(@"C:\Users\Fujitsu\Desktop\out.txt");
            GZipStream compStream = new GZipStream(sourceFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
        }
    }
}

