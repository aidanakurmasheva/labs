﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace saper2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Field field = new Field(10,10,10);
        Mybutton[,] b = new Mybutton[10, 10];
        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <field.n+1; i++)
            {
                for (int j = 1; j < field.m+1; j++)
                {
                    b[i,j] = new Mybutton(i, j);
                    b[i,j].Size = new Size(20, 20);
                    b[i,j].Location = new Point(i * 20, j * 20);
                    this.Controls.Add(b[i,j]);
                    b[i,j].Click += button_Click;
                }
            }
            
        }

        private void button_Click(object sender, EventArgs e)
        {
            Mybutton bt = sender as Mybutton;
            if (field.move(bt.x, bt.y) == -1)
            {
                bt.Text = "*";
            }
            else
            {
                if (field.move(bt.x, bt.y) != 0)
                {
                    bt.Text = (field.move(bt.x, bt.y)).ToString();
                }
                else if (field.move(bt.x, bt.y) == 0)
                {
                    for (int i = bt.x - 1; i < bt.x + 1; i++)
                    {
                        for (int j = bt.y - 1; j < bt.y + 1; j++)
                        {
                            b[i, j].Text = field.move(b[i, j].x, b[i, j].y).ToString();
                            if (field.move(b[i, j].x, b[i, j].y) == 0)
                            {
                                for (int s = b[i, j].x - 1; s < b[i, j].x + 1; s++)
                                {
                                    for (int t = b[i, j].y - 1; t < b[i, j].y + 1; t++)
                                    {
                                        b[s, t].Text = field.move(b[s, t].x, b[s, t].y).ToString();
                                    }
                                }
                            }

                        }

                    }

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}