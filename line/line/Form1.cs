﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace line
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Graphics g;
        int x = 320;
        int y = 100;
        int x1 = 400;
        int y1 = 100;
        public void button1_Click(object sender, EventArgs e)
        {
            
            g = this.CreateGraphics();
            Pen pen = new Pen(Color.DodgerBlue, 20);
            
            g.DrawLine(pen, x, y, x1, y1);

        }
        int p = 1;
        public void button2_Click(object sender, EventArgs e)
        {
            
            g.Clear(Color.White);
            Pen pen1 = new Pen(Color.DodgerBlue, 20);
            g.DrawLine(pen1, x - (p*5), y, x1 - (p*5), y1);
            x = x - p * 5;
            x1 = x1 - p * 5;
            p++;
            
        }
        int k = 1;
        public void button3_Click(object sender, EventArgs e)
        {

            
            g.Clear(Color.White);
            Pen pen2 = new Pen(Color.DodgerBlue, 20);
            g.DrawLine(pen2, x +(k*5), y, x1 +(k*5), y1);
            x = x + k * 5;
            x1 = x1 + k * 5;
            k++;
            
        }
    }
}
