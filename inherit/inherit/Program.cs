﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inherit
{
    public abstract class vecihle
    {
        public int price;
        public int speed;
        public int year;
        public vecihle(int _price, int _speed, int _year)
        {
            price = _price;
            speed = _speed;
            year = _year;
        }
        public override string ToString()
        {
            return price.ToString()+" "+speed.ToString()+" "+year.ToString();
        }
    }
    class car : vecihle
    {
        public car(int _price, int _speed, int _year):base(_price, _speed, _year)
        {
            
        }
    }
    class ship : vecihle
    {
        public int passengers;
        public string port;
        public ship(int _price,int _speed,int _year,int _passengers, string _port):base(_price,_speed,_year)
        {
            int passengers = _passengers;
            string port = _port;
        }
        public override string ToString()
        {
             return base.ToString()+" "+passengers.ToString() + " " + port;
        }
    }
    class plane : vecihle
    {
        public int passengers;
        public int heigth;
        public plane(int _price, int _speed, int _year, int _passengers, int _heigth): base(_price, _speed, _year)
        {
            int passengers = _passengers;
            int heigth = _heigth;
        }
        public override string ToString()
        {
            return base.ToString()+" "+ passengers.ToString()+" "+heigth.ToString();
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            car car = new car(30000, 150, 1996);
            Console.WriteLine(car);
            ship ship = new ship(300000, 33, 2010,120, "Hong Kong");
            ship.port = "Hong Kong";
            ship.passengers = 120;

            Console.WriteLine(ship);
            plane plane = new plane(10000000, 400, 2014, 250, 10000);
            plane.passengers = 250;
            plane.heigth = 10000;
            Console.WriteLine(plane);
            Console.ReadKey();
        }
    }
}
