﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace watcher
{
    class Program
    {
        static void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("{0}, {1}", e.ChangeType, e.FullPath);
        }
        static void Main(string[] args)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Filter = "*.txt";
            watcher.Path = @"C:\Users\Fujitsu\Desktop";
            watcher.Created += new FileSystemEventHandler(watcher_Changed);
            watcher.EnableRaisingEvents = true;
            Console.ReadKey();
        }
    }
}
